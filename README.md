# Ecommerce example for iTrellis

This is the example frontend for the iTrellis code review. This has been
kept separate for ease of deliverable. The other project included is the backend project, which is a C# WebAPI.

## Development server

Make sure to update environments/environment.ts, and change backendUrl to the appropriate network location of the .NET Web API backend.

Be sure to run `npm install` before attempting to serve, and have Angular CLI installed to run the dev server. You can install Angular CLI through npm: `npm install -g @angular/cli`.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Note

Please run the associated .NET Web API backend before attempting to run this project. If you get an Error message, it means it cannot contact the web service. (Graceful failure)