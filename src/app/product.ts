export class Product {
    productId: number;
    productName: string;
    inventoryQuantity: number;
    shipOnWeekends: boolean;
    maxDaysToShip: number;
    expectedShippingDate: Date;

    constructor(jsonObject)
    {
        this.productId = jsonObject.productId;
        this.productName = jsonObject.productName;
        this.inventoryQuantity = jsonObject.inventoryQuantity;
        this.maxDaysToShip = jsonObject.maxDaysToShip;
        this.expectedShippingDate = jsonObject.expectedShippingDate;
    }
}