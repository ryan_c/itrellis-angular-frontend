export class ShippingCalculatorRequest {
    public productId: number;
    public orderedOn: string;
}

export class ShippingCalculatorResponse {
    public productId: number;
    public expectedShipDate: string;
}