import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { DataService } from './data.service';

import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ErrorComponent } from './error/error.component';

const routes: Routes = [
  { path: 'product/:id', component: ProductComponent},
  { path: 'error', component: ErrorComponent },
  { path: '', component: ProductListComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    ProductComponent,
    ErrorComponent
  ],
  imports: [
    RouterModule.forRoot(routes, {enableTracing: true }),
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
