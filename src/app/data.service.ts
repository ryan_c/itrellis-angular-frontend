import { Injectable } from '@angular/core';
import { Product } from './product';
import { forEach } from '@angular/router/src/utils/collection';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { ShippingCalculatorRequest, ShippingCalculatorResponse } from './shipping';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  getAllProducts() {
    return this.http.get<Product[]>(environment.backendUrl + "products/products")
  }

  getProductById(id:number) {
    return this.http.get<Product>(environment.backendUrl + "products/product?productId=" + id);
  }

  calculateShippingDate(productId: number, orderDate: string)
  {
    var request = new ShippingCalculatorRequest();
    request.productId = productId;
    request.orderedOn = orderDate;

    return this.http.post<ShippingCalculatorResponse>(environment.backendUrl + "shipping/calculator", request);
  }

  constructor(private http: HttpClient) { }
}
