import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from '../product';
import { ShippingCalculatorResponse } from '../shipping';
import * as moment from 'moment';
import { Moment } from 'moment';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  public product: Product = null;
  public selectedDate: Moment = moment().utc();
  public expectedShipDate: Moment = moment().utc();
  public error: string = '';

  private selectedId: number;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.selectedId = +params.get('id');
      console.log(this.selectedDate.toISOString());

      this.dataService.getProductById(this.selectedId)
        .subscribe((data: Product) => {
          this.product = data;
          this.dataService.calculateShippingDate(this.product.productId, this.selectedDate.toISOString())
            .subscribe((data: ShippingCalculatorResponse) => this.expectedShipDate = moment(data.expectedShipDate),
              (error) => this.router.navigate(['/error']));
        },
          (error) => this.router.navigate(['/error']));
    });
  }

  calculateShippingDate(expectedOrder: string) {
    var newDate = moment(expectedOrder).utc();
    this.dataService.calculateShippingDate(this.product.productId, newDate.toISOString())
      .subscribe((data: ShippingCalculatorResponse) => this.expectedShipDate = moment(data.expectedShipDate),
        (error) => this.router.navigate(['/error']));
  }

}
