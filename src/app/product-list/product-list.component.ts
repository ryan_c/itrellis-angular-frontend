import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Product } from '../product';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  public productList: Product[] = [];
  public fetching: boolean = false;

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.fetching = true;
    this.dataService.getAllProducts()
      .subscribe(((data: Product[]) => {
        data.forEach(product => {
          this.productList.push(product);
        });
        this.fetching = false;
      }),
      (error) => this.router.navigate(['/error']));
  }

}
